POSTMAN 
---------------------
[X]GET REQUEST	- SHOW ALL USERNAMES  || 
    /api/users

---------------------------------
[X]GET REQUEST	- SHOW USER INFO    ||
    /api/user/{id}

id:		1
---------------------------------
[X]GET REQUEST	- SHOW CHARACTER INFO   ||
    /api/character/{id}

heroId:		1
---------------------------------
[X]GET REQUEST	- SHOW HEROCLASS INFO   ||
    /api/class/{className}

className:	Warrior

// Warrior  Ranger  Healer  
---------------------------------
[X]POST REQUEST 	- CREATING USER     ||
    /api/user

Body	-	JSON Object in POSTMAN

{
	"userName": "Andreas",
	"email": "Andreas@gmail.com"
}

---------------------------------
[X]POST REQUEST 	- CREATING CHARACTER    ||
    /api/character

UserId		// Who the hero belongs to
heroClassId	// Which class you want  1. Warrior	2. Ranger	3. Healer

userId":	1		
heroClassId":	1		
heroName :  	"SunFlower"